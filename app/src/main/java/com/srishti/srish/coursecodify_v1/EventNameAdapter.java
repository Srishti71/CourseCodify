package com.srishti.srish.coursecodify_v1;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.ArrayList;

/**
 * Created by User on 11/03/2018.
 */

public class EventNameAdapter extends ArrayAdapter {

    private Activity context;
    private ArrayList<String> eventName;


    public EventNameAdapter(Activity context,ArrayList<String> eventName) {
        super(context,  R.layout.eventname_layout_expandable, eventName);
        this.context = context;
        this.eventName = eventName;
    }

    @Override
    public View getView(int position, View view, ViewGroup parent) {

        LayoutInflater inflater = context.getLayoutInflater();
        view = inflater.inflate(R.layout.eventname_layout_expandable, null, true);
        TextView textView =  view.findViewById(R.id.eventName);
        textView.setText(eventName.get(position)+"");
        return view;
    }


}
