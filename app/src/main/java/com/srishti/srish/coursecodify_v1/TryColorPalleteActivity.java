package com.srishti.srish.coursecodify_v1;

import android.graphics.Paint;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;

public class TryColorPalleteActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_try_color_pallete);
        Paint mPaint;

        mPaint = new Paint();
        // on button click
        new ColorPickerDialog(TryColorPalleteActivity.this, new ColorPickerDialog.OnColorChangedListener() {
            @Override
            public void colorChanged(int color) {
                Log.i("color", "changed "+color);

            }
        }, mPaint.getColor()).show();
    }
}
