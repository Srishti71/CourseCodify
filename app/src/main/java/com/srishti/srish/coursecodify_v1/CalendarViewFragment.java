package com.srishti.srish.coursecodify_v1;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Context;
import android.content.SharedPreferences;
import android.net.Uri;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.v4.app.DialogFragment;
import android.support.v4.app.Fragment;
import android.text.format.Time;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Locale;


/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 *
 * to handle interaction events.
 * Use the  factory method to
 * create an instance of this fragment.
 */
public class CalendarViewFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        return new DatePickerDialog(getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
        // Do something with the date chosen by the user
        Calendar calendar = Calendar.getInstance();
        calendar.set(year,month,day);
        NavigationDrawerActivity.textView.setText(new SimpleDateFormat("MMMM dd, yyyy", Locale.US).format(calendar.getTimeInMillis()));

        Time time = new Time();
        time.set(00,00,01, day, month, year);
        final GetCalendarDetails getCalendarName = new GetCalendarDetails(getActivity());
        SharedPreferences sharedPreferences = PreferenceManager.getDefaultSharedPreferences(getContext());
        NavigationDrawerActivity.listOfEvents_Today.clear();
        NavigationDrawerActivity.listOfEvents_Today.addAll(getCalendarName.getevents(NavigationDrawerActivity.calendarNamesList.get(sharedPreferences.getInt("SelectedIndex", -1) - 1), time));
        ArrayAdapter listAdapter = new EventNameAdapter(getActivity(), NavigationDrawerActivity.listOfEvents_Today);
        NavigationDrawerActivity.listViewEvents.setAdapter(listAdapter);
    }
}