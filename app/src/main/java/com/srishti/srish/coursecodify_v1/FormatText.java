package com.srishti.srish.coursecodify_v1;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.preference.PreferenceManager;
import android.support.v7.app.AlertDialog;
import android.text.Spannable;
import android.text.style.RelativeSizeSpan;
import android.text.style.StyleSpan;
import android.text.style.UnderlineSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.SeekBar;
import android.widget.TextView;

import static android.content.Context.MODE_PRIVATE;

/**
 * Created by User on 13/03/2018.
 */

public class FormatText {
    float fs;
    Activity context;
    FormatText(Activity context){
        this.context = context;
    }
    void formatText(final EditText editText, String buttonname) {
        final SharedPreferences[] sharedPreferences = {PreferenceManager.getDefaultSharedPreferences(context)};

        int selectionStart = editText.getSelectionStart();
        int selectionEnd = editText.getSelectionEnd();

        if (selectionStart > selectionEnd) {
            int temp = selectionEnd;
            selectionEnd = selectionStart;
            selectionStart = temp;
        }


        if (selectionEnd > selectionStart) {
            final Spannable str = editText.getText();
            boolean exists = false;
            StyleSpan[] styleSpans;


            styleSpans = str.getSpans(selectionStart, selectionEnd, StyleSpan.class);

            switch (buttonname) {
                case "bold":
                    Log.i("Changing format to bold", "make it bold");
                    for (int i = 0; i < styleSpans.length; i++) {
                        if (styleSpans[i].getStyle() == android.graphics.Typeface.BOLD) {
                            str.removeSpan(styleSpans[i]);
                            exists = true;
                        }
                    }

                    if (!exists) {
                        str.setSpan(new StyleSpan(android.graphics.Typeface.BOLD), selectionStart, selectionEnd,
                                Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                    }
                    editText.setSelection(selectionStart, selectionEnd);
                    break;

                case "italic":
                    for (int i = 0; i < styleSpans.length; i++) {
                        if (styleSpans[i].getStyle() == Typeface.ITALIC) {
                            str.removeSpan(styleSpans[i]);
                            exists = true;
                        }
                    }


                    if (!exists) {
                        str.setSpan(new StyleSpan(Typeface.ITALIC), selectionStart, selectionEnd,
                                Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                    }
                    editText.setSelection(selectionStart, selectionEnd);
                    break;

                case "underline":
                    Log.i("Changing format to ", "underline");
                    for (int i = 0; i < styleSpans.length; i++) {
                        if (styleSpans[i].getStyle() == Paint.UNDERLINE_TEXT_FLAG) {
                            str.removeSpan(styleSpans[i]);
                            exists = true;
                        }
                    }

                    if (!exists) {
                        str.setSpan(new UnderlineSpan(), selectionStart, selectionEnd,
                                Spannable.SPAN_EXCLUSIVE_INCLUSIVE);
                    }
                    editText.setSelection(selectionStart, selectionEnd);
                    break;

                case "textResize":
                    final AlertDialog.Builder popDialog = new AlertDialog.Builder(context);

                    LayoutInflater inflater = context.getLayoutInflater();
                    View inflate=inflater.inflate(R.layout.texteditor_settextsize, null);
                    popDialog.setTitle("Please Select Your Desired Text Size ");
                    popDialog.setView(inflate);

                    SeekBar seekBar = (SeekBar) inflate.findViewById(R.id.seekbarTextSize);
                    final TextView startTextView = (TextView) inflate.findViewById(R.id.startpoint_seekbar);
                    TextView endTextView = (TextView) inflate.findViewById(R.id.end_point_seekbar);

                    AlertDialog alertDialog = popDialog.create();
                    alertDialog.show();
                    fs = sharedPreferences[0].getFloat("fontsize", 20);
                    seekBar.setProgress((int)fs);
                    editText.setTextSize(seekBar.getProgress());
                    final int finalSelectionStart = selectionStart;
                    final int finalSelectionEnd = selectionEnd;

                    seekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
                        @Override
                        public void onProgressChanged(SeekBar seekBar, int i, boolean b) {

                            if(fs < (float)i){
                                str.setSpan(new RelativeSizeSpan(1.2f), finalSelectionStart, finalSelectionEnd,
                                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                fs = (float)i;
                            }
                            else if(fs > (float)i){
                                str.setSpan(new RelativeSizeSpan(0.8f), finalSelectionStart, finalSelectionEnd,
                                        Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
                                fs = (float)i;
                            }

                            editText.setText(str);
                        }

                        @Override
                        public void onStartTrackingTouch(SeekBar seekBar) {

                        }

                        @Override
                        public void onStopTrackingTouch(SeekBar seekBar) {

                            sharedPreferences[0] = context.getPreferences(MODE_PRIVATE);
                            SharedPreferences.Editor ed = sharedPreferences[0].edit();
                            ed.putFloat("fontsize", editText.getTextSize());
                            ed.commit();
                        }
                    });

                    break;

                case "color":
                    Paint mPaint;

                    mPaint = new Paint();
                    // on button click
                   // new ColorPickerDialog(this, this, mPaint.getColor()).show();
                    break;


            }




        }
    }

}
