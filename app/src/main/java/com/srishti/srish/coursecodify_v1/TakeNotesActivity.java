package com.srishti.srish.coursecodify_v1;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Paint;
import android.media.MediaScannerConnection;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.TextView;
import android.widget.Toast;

import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.zip.Inflater;

public class TakeNotesActivity extends CourseCodifyHelper {

    EditText notesTitle;
    EditText notesBody;
    ArrayList<String> notesTitles = new ArrayList<String>();
    ArrayList<String> notesBodies = new ArrayList<String>();

    GetCalendarDetails getCalendarDetails = new GetCalendarDetails(TakeNotesActivity.this);
    CreateDirectories createDirectories = new CreateDirectories();
    String noteName;
    String notesContent;
    String currentSelected, selectedCurrentEvent;

    ArrayList<String> currentEvent = new ArrayList<String>();
    boolean createdEvent = false;
    private static final int REQUEST_FileAccess_PERMISSION = 300;
    FormatText formatText = new FormatText(this);

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_take_notes);
        this.getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        notesTitle = (EditText) findViewById(R.id.titleNotes);
        notesBody = (EditText) findViewById(R.id.notesBody);

        Intent intent = getIntent();
        noteName = intent.getStringExtra("NoteName");
        notesContent = intent.getStringExtra("NotesContent");
        currentSelected = intent.getStringExtra("CurrentSelected");

        Log.i("NoteId ", noteName + "");

        if (noteName != null) {
            Log.i("Notes returned", notesContent);
            notesTitle.setText(noteName);
            notesBody.setText(notesContent);
        } else {
            noteName = "";
            notesTitles.add("");
            notesBodies.add("");
        }

        ImageView bold = (ImageView) findViewById(R.id.bold);
        bold.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(notesBody.hasFocus())
                     formatText.formatText(notesBody, "bold");
                else if(notesTitle.hasFocus())
                    formatText.formatText(notesTitle, "bold");
            }
        });
        ImageView italic = (ImageView) findViewById(R.id.italic);
        italic.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(notesBody.hasFocus())
                    formatText.formatText(notesBody, "italic");
                else if(notesTitle.hasFocus())
                    formatText.formatText(notesTitle, "italic");
            }
        });
        ImageView underline = (ImageView) findViewById(R.id.underline);
        underline.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(notesBody.hasFocus())
                    formatText.formatText(notesBody, "underline");
                else if(notesTitle.hasFocus())
                    formatText.formatText(notesTitle, "underline");
            }
        });
        ImageView textResize = (ImageView) findViewById(R.id.textSize);
        textResize.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if(notesBody.hasFocus())
                    formatText.formatText(notesBody, "textResize");
                else if(notesTitle.hasFocus())
                    formatText.formatText(notesTitle, "textResize");

            }
        });
        ImageView colorpalatte = (ImageView) findViewById(R.id.color);
        colorpalatte.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              Intent intent1 = new Intent(TakeNotesActivity.this, TryColorPalleteActivity.class);
              startActivity(intent1);
            }
        });
        ImageView align = (ImageView) findViewById(R.id.align);
        ImageView fontFamily = (ImageView) findViewById(R.id.fontFamily);
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        getMenuInflater().inflate(R.menu.menu_take_notes, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == R.id.home) {
            this.finish();
            return true;
        }
        if (id == R.id.saveNotes) {

            if (notesTitle.getText().toString().equals("")) {
                Toast.makeText(getApplicationContext(), "Please give the notes Title", Toast.LENGTH_LONG).show();
            } else {

                if (noteName.contains(".txt")) {

                    if (currentSelected != null) {

                        createDirectories.createCourseCodifyFile();
                        createDirectories.createEventFolder(currentSelected);
                        createDirectories.createSubFolder(currentSelected, "Notes");
                        File newFile = createDirectories.saveMaterial(currentSelected, "Notes", notesTitle.getText().toString(), notesBody.getText().toString());
                        MediaScannerConnection.scanFile(TakeNotesActivity.this, new String[]{newFile.toString()}, null, null);
                        Intent intent = new Intent(TakeNotesActivity.this, AllListActivity.class);
                        intent.putExtra("Material", "Notes");
                        intent.putExtra("CalendarEvent", currentSelected);
                        startActivity(intent);
                        Toast.makeText(getApplicationContext(), "Saved here..", Toast.LENGTH_LONG).show();

                    } else {

                        Log.i("Selected Event", "is null");
                    }

                } else {
                    currentEvent.clear();
                    currentEvent = getCalendarDetails.getCurrentEvent();
                    if (currentEvent.isEmpty()) {

                        askPermissionToGoToCalendar();
                        createdEvent = true;

                    } else {
                        if (currentEvent.size() > 1) {

                            AlertDialog.Builder builder = new AlertDialog.Builder(TakeNotesActivity.this);
                            builder.setTitle("Choose the event To Save Images");
                            final ArrayAdapter<String> arrayAdapter = new ArrayAdapter<String>(
                                    this,
                                    android.R.layout.simple_list_item_1,
                                    currentEvent);

                            builder.setSingleChoiceItems(arrayAdapter, -1,
                                    new DialogInterface.OnClickListener() {
                                        @Override
                                        public void onClick(DialogInterface dialogInterface, int i) {
                                            selectedCurrentEvent = currentEvent.get(i);
                                            Toast.makeText(getApplicationContext(), "Selected Event " + selectedCurrentEvent, Toast.LENGTH_LONG).show();

                                        }
                                    });

                            builder.setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {
                                    save();
                                    Intent intent = new Intent(TakeNotesActivity.this, AllListActivity.class);
                                    intent.putExtra("Material", "Notes");
                                    intent.putExtra("CalendarEvent", selectedCurrentEvent);
                                    startActivity(intent);
                                    Toast.makeText(getApplicationContext(), "Saved here..", Toast.LENGTH_LONG).show();
                                }
                            });
                            final AlertDialog alertDialog = builder.create();
                            alertDialog.show();

                        } else {

                            selectedCurrentEvent = currentEvent.get(0);

                            save();
                            Intent intent = new Intent(TakeNotesActivity.this, AllListActivity.class);
                            intent.putExtra("Material", "Notes");
                            intent.putExtra("CalendarEvent", selectedCurrentEvent);
                            startActivity(intent);
                            Toast.makeText(getApplicationContext(), "Saved here..", Toast.LENGTH_LONG).show();
                        }


                    }

                }
            }
        }

        if (id == R.id.goToViewNotes) {
            Intent intent = new Intent(TakeNotesActivity.this, AllListActivity.class);
            intent.putExtra("Material", "Notes");
            startActivity(intent);

        }

        if (id == R.id.refresh) {
            Intent intent1 = getIntent();
            finish();
            startActivity(intent1);
        }
        return super.onOptionsItemSelected(item);
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        if (requestCode == REQUEST_FileAccess_PERMISSION) {
            if (grantResults[0] == PackageManager.PERMISSION_DENIED) {

                Toast.makeText(TakeNotesActivity.this, "Sorry!!!, you can't use this app without granting permission", Toast.LENGTH_LONG).show();
                finish();
            }
        }
    }

    @Override
    protected void onResume() {
        super.onResume();

    }

    public void save() {
        createDirectories.createCourseCodifyFile();
        createDirectories.createEventFolder(selectedCurrentEvent);
        createDirectories.createSubFolder(selectedCurrentEvent, "Notes");
        File notesName = createDirectories.saveMaterial(selectedCurrentEvent, "Notes", notesTitle.getText().toString() + ".txt", notesBody.getText().toString());
        MediaScannerConnection.scanFile(TakeNotesActivity.this, new String[]{notesName.toString()}, null, null);
    }

}
