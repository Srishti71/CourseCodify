package com.srishti.srish.coursecodify_v1;

import android.Manifest;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.format.Time;
import android.util.Log;
import android.view.MenuInflater;
import android.view.View;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.CalendarView;
import android.widget.ExpandableListView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import org.w3c.dom.Text;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import static android.content.pm.PackageManager.PERMISSION_GRANTED;
import static android.widget.AdapterView.*;


public class NavigationDrawerActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private static final String TAG = "hello";
    static ArrayList<String> listOfEvents_Today = new ArrayList<String>();
    static List<String> calendarNamesList = new ArrayList<String>();
    static ListView listViewEvents;
    private static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 1;
    static TextView textView;
    SharedPreferences sharedPreferences;
    boolean firstInstallation = true;
    static String selectedCalendar;
    CreateDirectories create = new CreateDirectories();
    final GetCalendarDetails getCalendarName = new GetCalendarDetails(NavigationDrawerActivity.this);
    ArrayAdapter arrayAdapter;
    CreateDirectories createDirectories = new CreateDirectories();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_navigation_drawer);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        navigationView.setNavigationItemSelectedListener(this);


        if ((ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_CALENDAR) != PERMISSION_GRANTED) ||
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_CALENDAR) != PERMISSION_GRANTED) ||
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.RECORD_AUDIO) != PERMISSION_GRANTED) ||
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) != PERMISSION_GRANTED) ||
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PERMISSION_GRANTED) ||
                (ActivityCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PERMISSION_GRANTED))

        {
            ActivityCompat.requestPermissions(this,
                    new String[]{
                            Manifest.permission.READ_CALENDAR,
                            Manifest.permission.RECORD_AUDIO,
                            Manifest.permission.WRITE_EXTERNAL_STORAGE,
                            Manifest.permission.READ_EXTERNAL_STORAGE,
                            Manifest.permission.CAMERA}, MY_PERMISSIONS_REQUEST_READ_CONTACTS);

        }

        View hView = navigationView.getHeaderView(0);
        listViewEvents = (ListView) findViewById(R.id.listOfEvents);

        listViewEvents.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapterView, View view, final int i, long l) {

                if(createDirectories.readAllDirectoryName(null, null).contains(listOfEvents_Today.get(i))){
                    Intent intent = new Intent(view.getContext(), AllListActivity.class);
                    intent.putExtra("CalendarEvent", listOfEvents_Today.get(i));
                    intent.putExtra("Material", "All Materials");
                    startActivity(intent);
                }
                else
                {
                    Toast.makeText(getApplicationContext(),"No materials available",Toast.LENGTH_SHORT).show();
                }


            }

        });


        ImageView camera = (ImageView) findViewById(R.id.cameraButton);
        camera.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeImages();
            }
        });

        ImageView notes = (ImageView) findViewById(R.id.notepadbutton);
        notes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeNotes();
            }
        });

        ImageView record = (ImageView) findViewById(R.id.microphonebutton);
        record.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                takeRecordings();
            }
        });

        ImageView materials = (ImageView) findViewById(R.id.materialsbutton);
        materials.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                viewMaterials();
            }
        });


        textView = (TextView) findViewById(R.id.date);

        textView.setText(getTodaysDate());
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.setting_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        switch (item.getItemId()) {
            case R.id.settings:
                Intent intent = new Intent(NavigationDrawerActivity.this, SettingActivity.class);
                startActivity(intent);
                break;

            case R.id.refresh:
                refresh();

            case R.id.calendarIcon:
                Log.i("Calendar", "pressed");
                DialogFragment newFragment = new CalendarViewFragment();
                newFragment.show(getSupportFragmentManager(), TAG);
            default:
                break;
        }

        return super.onOptionsItemSelected(item);
    }

    void refresh() {
        Intent intent1 = getIntent();
        finish();
        startActivity(intent1);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.nav_camera) {

            takeImages();

        } else if (id == R.id.nav_notes) {
            takeNotes();

        } else if (id == R.id.nav_audioRecord) {
            takeRecordings();

        } else if (id == R.id.nav_setting) {
            Intent intent = new Intent(NavigationDrawerActivity.this, SettingActivity.class);
            startActivity(intent);

        } else if (id == R.id.nav_material) {
            viewMaterials();

        }


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }


    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS:

                if (grantResults.length > 0
                        && grantResults[0] == PERMISSION_GRANTED) {

                    Log.i("Permission Granted", "Permission Granted");


                } else {
                    if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_CALENDAR)) {
                        new AlertDialog.Builder(this)
                                .setTitle("Read Contact Permission")
                                .setMessage("You need to grant read calendar Permission. Press the \"Refresh\" button").show();
                    } else {


                        new AlertDialog.Builder(this)
                                .setTitle("Read Contact Permission Denied")
                                .setMessage("You need to grant read calendar Permission Denied").show();
                    }

                }
                break;

        }

    }


    @Override
    protected void onResume() {
        Log.i("Resume", "created");
        super.onResume();

        sharedPreferences = PreferenceManager.getDefaultSharedPreferences(NavigationDrawerActivity.this);
        firstInstallation = sharedPreferences.getBoolean("FIRST_RUN", false);

        if (!firstInstallation) {
            sharedPreferences.edit().putBoolean("FIRST_RUN", true).commit();
            create.createCourseCodifyFile();

            AlertDialog.Builder goToSettings = new AlertDialog.Builder(NavigationDrawerActivity.this);
            goToSettings.setMessage("Go to Settings to choose Calendar?")
                    .setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            calendarNamesList.clear();
                            calendarNamesList.addAll(getCalendarName.getAllCalendarName());

                            Intent settingsIntent = new Intent(NavigationDrawerActivity.this, SettingActivity.class);
                            startActivity(settingsIntent);
                        }
                    })
                    .setNegativeButton("Not Now", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {

                        }
                    });
            AlertDialog alertForSettingsConfig = goToSettings.create();
            alertForSettingsConfig.show();
        }
        textView.setText(getTodaysDate());
        calendarNamesList.clear();
        calendarNamesList.addAll(getCalendarName.getAllCalendarName());
        if (!calendarNamesList.isEmpty()) {
            Log.i("sharedP selectedIndex", (sharedPreferences.getInt("SelectedIndex", -1) - 1) + "");
            listOfEvents_Today.clear();
            listOfEvents_Today.addAll(getCalendarName.getevents(calendarNamesList.get(sharedPreferences.getInt("SelectedIndex", -1) - 1), null));
            //directoryExpandableListAdapter = new DirectoryExpandableListAdapter(NavigationDrawerActivity.this, listOfEvents_Today, listOfSubDirectory);
            arrayAdapter = new EventNameAdapter(this, listOfEvents_Today);
            selectedCalendar = calendarNamesList.get(sharedPreferences.getInt("SelectedIndex", -1) - 1);
            listViewEvents.setAdapter(arrayAdapter);
        }


    }

    @Override
    protected void onRestart() {
        Log.i("Restarts", "created");
        super.onRestart();


    }

    @Override
    protected void onPause() {
        super.onPause();
        Log.i("OnPause", "restart");

    }

    public void takeImages() {

        Intent intent = new Intent(NavigationDrawerActivity.this, TakeImagesActivity.class);
        startActivity(intent);
    }

    public void takeNotes() {

        Intent intent = new Intent(NavigationDrawerActivity.this, TakeNotesActivity.class);
        startActivity(intent);
    }

    public void takeRecordings() {

        Intent intent = new Intent(NavigationDrawerActivity.this, RecordingActivity.class);
        startActivity(intent);

    }


    public void viewMaterials() {

        Intent intent = new Intent(NavigationDrawerActivity.this, AllListActivity.class);
        intent.putExtra("Material", "All Materials");
        startActivity(intent);

    }

    public String getTodaysDate() {
        final Calendar c = Calendar.getInstance();
        String date = new SimpleDateFormat("MMMM dd, yyyy", Locale.US).format(c.getTimeInMillis());
        return date;
    }
}
